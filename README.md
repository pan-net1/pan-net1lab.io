# Howto edit landing page

Main topic on the landing page can be edited in `components/Hero.vue` and `components/HeroContent.vue`
Content blocks under can be find in `components/Teasers.vue` and `components/TeasersColumn.vue`
"Our CI/CD story" section is located in `components/Features.vue`. 

## Content

For adding the content, the Nuxt is using [content](https://content.nuxtjs.org/) modul. It can render Markdown, JSON, YAML, XML and CSV files which are stored in `content` directory. You can check `pages/intro.vue` for an example. In the `dev` mode the content can be edited directly on the web page which can be handy.  

## Build Setup

Install `yarn` on your laptop. Clone this repo and enter the repo root directory.

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000/nuxt/landing-page
$ yarn dev
```
