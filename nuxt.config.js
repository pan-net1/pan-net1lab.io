import {readFileSync} from 'fs'

export default {
  target: 'static',
  env: {
    stage: process.env.STAGE || false,
    commit_id: process.env.COMMIT_HASH,
    branch: process.env.REF_NAME
  },
  router: {
    base: process.env.ENV_SUB_PATH || '/'
  },
  head: {
    title: process.env.npm_package_description || '',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {
        hid: 'description',
        name: 'description',
        content: 'Learn what CI/CD is and how it can help you deliver software faster and manage it in a more efficient way'
      }
    ],
    link: [{rel: 'icon', type: 'image/x-icon', href: '/favicon.png'}]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {color: '#fff'},
  /*
   ** Global CSS
   */
  css: [
    '~/assets/css/main.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {src: '~plugins/vue-tree-navigation', ssr: false},
    '~/plugins/vue-scrollactive.min'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  components: true,
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    'nuxt-i18n',
    {
      src: "@nuxtjs/lunr-module",
      options: {
        placeholderText: "Search"
      },
    },
    '@nuxt/content',
    "@nuxtjs/tailwindcss",
    [
      'nuxt-fontawesome', {
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['fas']
        },
        {
          set: '@fortawesome/free-brands-svg-icons',
          icons: ['fab']
        },
        {
          set: '@fortawesome/free-regular-svg-icons',
          icons: ['far']
        }
      ]
    },
      '@nuxtjs/svg',
    ],
  ],
  i18n: {
    detectBrowserLanguage: false,
    locales: ['en'],
    defaultLocale: 'en',
    strategy: 'prefix_and_default',
    vueI18n: {
      messages: {
        en: {
          'lunr-module': {
            placeholderText: 'Search'
          }
        }
      }
    }
  },
  content: {
    markdown: {
      prism: {
        theme: 'prism-themes/themes/prism-material-light.css'
      }
    }
  },
  purgeCSS: {
    whitelist: ['hidden'],
    whitelistPatterns: [/md:w-[1-6]/]
  },
  /*
   ** Build configuration
   */
  build: {
    publicPath: (process.env.ENV_SUB_PATH || '/') + '_nuxt'
  },
  /*
   ** Indexation configuration
   */
  hooks: {
    ready(nuxt) {

      const fs = require("fs");
      const parseMD = require('parse-md').default;

      let directory_name = __dirname + '/content/articles';

      nuxt.callHook('lunr:document', {
        document: true
      });
      let filenames = fs.readdirSync(directory_name);
      let index = 1;

      filenames.forEach((file) => {
        index++;
        const doc = readFileSync(__dirname + '/content/articles/' + file, 'utf-8');
        const {metadata, content} = parseMD(doc);
        let linesOfDoc = content.split('\n');
        let indexObject = "";
        let currentTitle = "";

        function callHook() {
          currentTitle = currentTitle.charAt(0).toUpperCase() + currentTitle.slice(1);
          indexObject = indexObject.toLowerCase() + " " + currentTitle;
          let multipliedIndexObject = "";
          let words = indexObject.split(" ");
          for (let j = 0; j < words.length; j++) {
            let multipliedWord = "";
            for (let symb = 0; symb < words[j].length; symb++) {
              multipliedWord += words[j][symb];
              multipliedIndexObject += multipliedWord + " ";
            }
          }
          multipliedIndexObject += indexObject;

          const document = {
            id: index,
            title: currentTitle,
            body: multipliedIndexObject
          };

          const titleWords = currentTitle.split(" ");
          let titleHref = titleWords[0].toLowerCase();
          for (let titleIndex = 1; titleIndex < titleWords.length; titleIndex++) {
            titleHref += "-" + titleWords[titleIndex].toLowerCase();
          }
          titleHref = titleHref.replace(/[\/,.)(\s]/g, '')
          const meta = {
            href: "/" + metadata.href + "#" + titleHref,
            title: currentTitle,
            page: metadata.title,
          };

          nuxt.callHook('lunr:document', ({
            locale: "en",
            document: document,
            meta: meta
          }));
          index++;
        }

        for (let i = 0; i < linesOfDoc.length; i++) {
          if (linesOfDoc[i].length > 0 && linesOfDoc[i][0] === '#') {
            if (indexObject.length !== 0 && currentTitle !== "") {
              callHook();
            }
            indexObject = "";
            let line = linesOfDoc[i];
            while (line.length > 0 && (line[0] === '*' || line[0] === '#' || line[0] === ' ')) {
              line = line.substr(1, line.length)
            }
            while (line.length > 0 && (line[line.length - 1] === ":" || line[line.length - 1] === '*' || line[line.length - 1] === ' ')) {
              line = line.substr(0, line.length - 1)
            }
            currentTitle = line;
          } else if (linesOfDoc[i].length > 0) {
            indexObject += linesOfDoc[i] + " "
          }
        }
        callHook();
      });

    }
  },
  generate: {
    dir: 'public'
  }
}