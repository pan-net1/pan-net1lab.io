export default async function (to, from, savedPosition) {
    // If the returned position is falsy or an empty object, will retain current scroll position
    const isRouteChanged = to !== from

    // savedPosition is only available for popstate navigations (back button)
    if (savedPosition) {
        return savedPosition
    } else if (isRouteChanged && (to.matched.some((r) => r.components.default.options.scrollToTop))) {
        // if one of the children has scrollToTop option set to true
        return {x: 0, y: 0, behavior: 'smooth', offset: {x: 0, y: 0}}
    }

    const findElBySelector = async (hash, x) => {
        return document.querySelector(hash) ||
            new Promise((resolve, reject) => {
                if (x > 50) {
                    return resolve()
                }
                setTimeout(() => {
                    resolve(findElBySelector(hash, ++x || 1))
                }, 100)
            })
    }

    if (to.hash) {
        let el = await findElBySelector(to.hash)
        return {
            x: 0,
            y: el.offsetTop - 92,
            behavior: 'smooth'
        }

    }

    if (window.innerWidth < 1024) {

        return {
            x: 0,
            y: 582,
            behavior: 'smooth'
        }

    }
    return {
        x: 0,
        y: 0,
        behavior: 'smooth'
    }
}