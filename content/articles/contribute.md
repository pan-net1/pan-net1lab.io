---
title: Contribute
href: "contribute"
---

# How to contribute

This document provides information on how to contribute to Pan-Net DevOps documentation.

This web resource was born as an aggregation of our experience in managing the application lifecycle with CI/CD. But not only. Our goal is to bring together like-minded people interested in modern methods of Development, Deployment and Operations of Cloud Applications. To encourage the exchange of knowledge within the community and find the best tools and approaches to meet the challenges. Therefore, we highly welcome any feedback, comments and contribution. 

If you have an idea how to improve this documentation, there are two ways to do it:
* Use the [feedback form](../feedback). We will process your request, create an issue, if needed, and inform you about the progress. 
* Use the instruction below to make changes yourself 


## General contribution flow

The general flow for contributing to the Pan-Net DevOps repository is:  
1. Request access to the project using the link on the top of the project [overview page](https://gitlab.com/pan-net1/pan-net1.gitlab.io) and wait for access to be granted. 
1. Choose a topic to work on.
1. Open an issue in [Gitlab](https://gitlab.com/pan-net1/pan-net1.gitlab.io/-/issues). 
1. Assign yourself to the issue.
1. Create a brunch using a drop-down menu in front of the `Create merge request` button. Pls, choose branch `develop` as a source. This will automatically create a branch with the issue number and title as branch name e.g. : 1-example-issue.
1. Work on the feature. You can use [staging repository](https://gitlab.com/pan-net1/pan-net1-stage) when you want to test changes online.  
1. Open Merge Request to `develop` branch. Squashing commits is recommended.
1. Maintainer will review your MR and approve merge to `develop` branch.  
1. Maintainer will create MR and merge `develop` branch to `master`.


## Recommendations 

When adding or reviewing new topics make sure you:

1. Write a clear, short, and on point sentences
1. Visually differentiate code snippets so the user can copy/paste them
1. Include all dependencies and prerequisites user might need for your example to work
1. Test your code snippets on a vanilla system


## Licensing

The Pan-Net DevOps documentation is available under the Creative Commons Share-Alike 4.0 International (CC BY-SA 4.0). This license allows us to ensure that this knowledge remains free and open while encouraging contribution and authorship.

For more information regarding the license and its usage please go here: https://creativecommons.org/licenses/by-sa/4.0/
