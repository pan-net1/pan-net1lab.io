---
title:  "Lifecycle Management"
href: "lcm"
created: "2020-10-20"
---

# Lifecycle Management

Lifecycle Management enables you to perform automated operations on your application such as deployment, scaling, healing, upgrade, migration, termination and so on.
We went through a lot of brainstorming sessions and we were trying a lot of options in Pan-Net to find the best software which will manage lifecycle operations or orchestrate application according our needs and conditions. And this journey is not finished yet.

It has became easier since new robust and stable technologies came out (e.g. Kubernetes bundled with ArgoCD, for example) but if a customer is interested into VM based deployment then it's not that easy and there is not one preferred tool which can meet all the requirements.

So then we can split the management of lifecycle operations to two ways:

- **Virtual Machine-based deployments**
- **Container-based deployments**

<br>

## Virtual Machine-based deployments

We were experiencing several solutions but the last attempt developed by Pan-Net was ALiEn, which can be very useful to manage lifecycle operations.

### ALiEn

Application Lifecycle Engine (ALiEn) is set of open source tools built in Pan-Net CI/CD toolchain for automated application delivery.
ALiEn doesn’t require knowledge of any proprietary technologies or specific standards, you just need basic DevOps skills and basic knowledge of Terraform and Ansible.
ALiEn is distributed as a single Docker image. Besides ALiEn the image contains also other binaries you may need for your deployment (`ansible`, `git`, `jfrog`, `terraform`, `vault`, etc.).

#### ALiEn features:

- Repeatable deployments — you can easily deploy your application in multiple Data Centers or multiple environments inside the same Data Center

- Application scaling — you can easily scale any cluster inside your application adding or removing nodes by running simple pipeline from Gitlab UI or Gitlab API

- Application healing — you can rebuild any faulty VM inside your application by simply running a pipeline from Gitlab UI or Gitlab API

- Application upgrade/modification — you can make any changes in your app, ALiEn will identify the diff and implement only required changes on the running application instance

- Multi-stage pipeline — automated strategy leading your application through all phases of onboarding — dev, QA, staging, production

- Rolling upgrade — ALiEn can perform rolling upgrade for clusters inside your application step-by-step without affecting your service

- Rollback — you can always rollback to the previous version if something goes wrong

- Automation of scaling and healing — ALiEn can be integrated with application monitoring component to trigger scaling and healing automatically

ALiEn docker image is stored in Pan-Net Artifactory inventory - contact us in case you would like to test or use ALiEn.

## Container based deployments

The most common open source system for automating deployment, scaling, and management of containerized applications is Kubernetes.
The complete Kubernetes documentation can be found [**here**](https://kubernetes.io/docs/home/).
To ensure continuous delivery for your application you need to set up some upper layer which can better operate lifecycle management for your application.

**Here are some useful open source tools which can improve your CI/CD design:**
<br>

### Argo CD

Complete documentation to Argo CD - Declarative GitOps CD for Kubernetes can be found [**here**](https://argoproj.github.io/argo-cd/).

### Tekton

Complete documentation to Tekton - Cloud native CI/CD can be found [**here**](https://tekton.dev/).

### Jenkins X

Complete documentation to Jenkins X [**here**](https://jenkins-x.io/).

### Helm

Seen as "package manager for Kubernetes", Helm is a good balance for better installing, managing or upgrading even the most complex Kubernetes application.
Helm Charts are easy to create, version, share, and publish — it motivates reusability so teams can stop copy-and-pasting.
Complete documentation to Helm - Package manager for Kubernetes can be found [**here**](https://helm.sh/).
