---
title: Prerequisites
href: "prerequisites"
created: "2020-10-19"
---

# Prerequisites

To start an application deployment we should understand the resources demand (number of nodes (VMs or pods), CPU, RAM, etc.) of the application based on the information obtained from the application owner/vendor, taking availability requirements into account (Disaster Recovery, High-Availability, Geo-Redundancy, etc.).

## Make sure that application has sufficient documentation describing  

**for VNF**

* Application topology - the amount and types of VMs (nodes), networks, storages, other environment objects, communication matrix

* Network security rules

* Affinity/Anti-Affinity requirements

* Application configuration (Described for Ansible, Chef, Saltstack, Puppet, etc.)

* Application lifecycle operations sequences

* Target architecture for each environment

* Security requirements (secrets Management, separation of `Prod` and `Test` environments or private runners, security hardening, etc.)  

**for CNF**

* Application topology - the amount and types of pods (StatefulSets, DaemonSets ), storages, other environment objects, communication matrix  

* Network configuration and Network Policies ( Services )

* Target architecture for each environment (Network setup, worker nodes resources, etc. )  

* Deployments Affinity/Anti-Affinity specs

* Configuration options (ConfigMaps, workloads, secrets, etc.)

* Deployments lifecycle sequences.  

* Security requirements (secrets Management, separation of `Prod` and `Test` environments, security hardening, etc.)  


\* Most of the questions may be answered by provided Helm chart. If you don't have prepared and tested charts (or analogs) your team probably will need to write configuration from scratch. And to do this you will need to clarify information about the target application, pointed in the list above.


## Make sure that you have a DevOps engineer in your team with the following skills  

**for VNF**

* Basic understanding of CI/CD concept

* Knowledge of software development applications (e.g. Gitlab, Github, Bitbucket, etc.) that enables Concurrent DevOps, making the software lifecycle faster and radically improving the speed of business

* Knowledge of OpenStack

* Knowledge of configuration management tools (Ansible, Chef, Puppet, etc.)

* Understanding of Infrastructure as Code concept  

**for CNF**

* Understanding of Infrastructure as Code concept

* Basic understanding of CI/CD concept

* Knowledge of software development applications (e.g. Gitlab, Github, Bitbucket, etc.) that enables Concurrent DevOps, making the software lifecycle faster and radically improving the speed of business

* Knowledge of Kubernetes  

  -  Kubernetes networking 

  -  container runtime (containerd, Docker, CRI-O)

  -  management of Kubernetes workloads ( Kubernetes YAML manifests, Helm, Kustomize, etc.)

  -  continuous deployment tools ( Argo CD, Flux, Jenkins X, Tekton ... )  

## Make sure you have some services available for smooth operations

* Certificate Authority with support for full certificate automation lifecycle (issuing, renewal, revocation)
* Secret Management service
* Log Management platform
* Metrics and Observability platforms

## Vendor requirements

If you're a vendor, you should take a look at the following requirements:

* <nuxt-link to="/requirements/RequirementsForApplications_CIT2.0.xlsx" target="_blank">Application Requirements for CIT v2.0</nuxt-link>
* <nuxt-link to="/requirements/RequirementsForApplications_CIT1.0.xlsx" target="_blank">Application Requirements for CIT v1.0</nuxt-link>
