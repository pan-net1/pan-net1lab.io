---
title:  "Introduction to CI/CD"
href: "intro"
created: "2020-11-05"
---

# What is CI/CD

The acronym CI/CD (sometimes CI/CT/CD or CI/CDD) stands for Continuous Integration, Continuous Testing, Continuous Delivery and Continuous Deployment. CI/CD represents processes and tools around constantly integrating, delivering and deploying new code (features, bug fixes, patches). It involves continuously building, testing, and deploying code changes at every small iteration - reducing the chance of developing new code based on bugged or failed previous versions - to end users.

<center><img width="60%" src="cicd-transparent-magenta.png"/></center>  
<br>

**Continuous Integration (CI)** focuses on blending the code of individual developers into a repository. Every push to the repository may execute the CI pipeline - a set of scripts to build and test the application automatically.

**Continuous delivery (CD)** is a step in the pipeline, which continuously deploys the application and eventually it may include the deployment of the whole infrastructure in the cloud described in Infrastructure as Code (IaC). Here the deployment is triggered manually. This method ensures the code is checked automatically but requires human intervention to manually and strategically trigger the deployment of the changes.

**Continuous deployment (CD)** deploys the application and IaC automatically, thus here the human intervention is not needed.  

## CI/CD tools used in Pan-Net

CI/CD practices will require you to use various tools for different purpose.

It's safe to say the main tool used for CI/CD in DT Pan-Net is **GitLab**: leveraging mostly source code repository, version control system and GitLab Runners to execute CI pipelines.

For IaC and and infrastructure management on OpanStack cloud the tools of choice are **HashiCorp Terraform** or **OpenStack HEAT**.

For configuration automation most teams use **Ansible**. Additional tools serving various purpose are **HashiCorp Vault** (secrets store), **JFrog Artifactory** (packages repository), **HashiCorp Packer** (building VM images) and in-house built **ALiEn** (deployment automation and lifecycle management), among other tools.

## Example of CI/CD processes

Imagine you have a code repository in GitLab. The repository consist of IaC describing your cloud infrastructure (Virtual Machines, Networks, Security Groups, Volumes etc.), configuration code (for example Ansible playbooks used for software configuration inside of the Virtual Machines) and your application code.  

You are willing to create a new feature to the application, the steps you could take are:

* Clone the repository to local PC
* Create a feature branch from the main branch

```bash
git checkout -b your-feature-branch
```
* Code the new feature, stage it for change and commit the changes to the feature branch
```bash
git add <file-changed>
git commit -m 'add support for feature xzy'
```
* Push the changes into the remote repository in GitLab
```bash
git push --set-upstream origin your-feature-branch
```
* If configured accordingly, GitLab will run the **CI pipeline**, where application will be build and the unit and integrity tests will run.
* If the tests will succeed, automation proceeds to the **CD pipeline** stage into his `dev` environment. This will deploy the virtual infrastructure from the IaC, run the installation and configuration of the application.
* Now the changes can be reviewed in the dev environment.
* If everything went well and the new feature works fine, you will create a merge request where his repository branch will be merged to the main branch. The feature branch can be deleted.
* Once the merge request is approved and changes are merged, another pipeline will deploy the changes to the staging environment.
* After reviewing the application in the staging environment, the main branch can be tagged with a new release tag. if configured so, Tagging will trigger the execution of the pipeline where the changes will be (usually after triggering the pipeline stage manually) deployed into the production environment.

 <center><img width="100%" src="cicd-diagram.png"/></center>

## Multistage CI/CD pipeline

In this video you will see an example how the process of CI/CD may look like in combination with Gitlab Flow branching model and GitOps methodology.

<br>
<div class="resp-container">
  <iframe 
    src="https://www.youtube.com/embed/t1QFkZbTfkE" 
    frameborder="0" __
    allow="encrypted-media; picture-in-picture"
    allowfullscreen>
  </iframe>
</div>
