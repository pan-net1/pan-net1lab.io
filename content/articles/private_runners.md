---
title: "Private CI runners"
href: "private_runners"
created: "2020-11-06"
---

# Private CI runners

Also known as "Specific Runners", they are required for projects which use secrets or other data which is supposed to remain safe. Read more about it [here](https://docs.gitlab.com/ee/ci/runners/README.html#specific-runners).

Private Runner deployment should be isolated from Application deployment so both deployments are living their separate lives and they can be maintained properly without affecting each other.
There are several options on how or where to install a GitLab runner, the most common being virtual servers/machines or directly to Kubernetes. 
The official Gitlab installation procedures can be found [**here**](https://docs.gitlab.com/runner/install/).

## Requirements

Make sure you have gone through the [requirements](https://docs.gitlab.com/runner/register/#requirements) before proceeding.

## Private runners on VM

For a simple example of GitLab runner installation and configuration, terraform and Ansible playbooks can be used.
The private runner configuration and registration can vary on implementation, but here is example of an ansible playbook:

```yaml
---
- name: Read runner reg. token from env
  hosts: runners
  tasks:
    - set_fact:
        gitlab_reg_token: "{{ lookup('env', 'GIT_REG_TOKEN') }}"

- name: Download gitlab runner package
  hosts: runners
  become: yes
  vars:
      gitlab_runner_coordinator_url: https://gitlab.tools.in.pan-net.eu/
      gitlab_runner_registration_token: "{{ gitlab_reg_token }}"
      gitlab_runner_tags: ["ghost","cache"]
      gitlab_runner_executor: docker
      gitlab_runner_docker_image: artifactory.tools.in.pan-net.eu/docker/alpine:latest
      gitlab_runner_run_untaged: false

  tasks:
    - name: download gitlab runner package
      get_url:
        url: https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
        dest: /home/ubuntu/
        use_proxy: yes

    - name: install gitlab runner package
      apt:
        deb: /home/ubuntu/gitlab-runner_amd64.deb
        state: present

    - name: Check if runner is registered with gitlab
      command:
        gitlab-runner verify
      register: runner_status
      changed_when: False
      failed_when: False
      check_mode: False

    - name: Register runner with GitLab
      command: gitlab-runner register >
        --non-interactive
        --url '{{ gitlab_runner_coordinator_url }}'
        --registration-token '{{ gitlab_runner_registration_token }}'
        --description '{{ ansible_hostname }}'
        --tag-list '{{ gitlab_runner_tags | join(",") }}'
        --executor '{{ gitlab_runner_executor }}'
        --docker-image '{{ gitlab_runner_docker_image }}'
        --run-untagged={{ gitlab_runner_run_untaged }}
      when:
        - runner_status.stderr is not search('Verifying runner... is alive')
```
<br>

Variables used for proper set up of Gitlab Runners are defined as `vars` in the above ansible playbook example.

## Private runners on Kubernetes

The easiest way to install GitLab Runner is by using the Helm Chart.

All you need is your Gitlab URL and the runner registration token; adjust the `values.yaml` file accordingly and follow the step by step procedure which can be found on the official Gitlab documentation page [**here**](https://docs.gitlab.com/runner/install/kubernetes.html)

