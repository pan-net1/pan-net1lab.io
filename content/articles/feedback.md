---
title: Feedback
href: "feedback"
---

# Contact us

We are highly welcome to any kind of feedback, comments and contribution. 

Use this form to 

* **Share your impressions or leave feedback**  
    It is useful and welcome

* **Make small fixes**   
    If you spot a typo, some other incorrectness or error.

* **Share your team experience**  
    You have an interesting experience in implementing DevOps practices? It can help others in their work. 

* **Change the content**  
    Propose a new topic or extend existing one if you don’t find information you are interested in.

* **Complain**  
    You think that this resource offends your feelings and should be deleted from the Internet immediately.

* **To contact us**  
    For any other topics you want to talk about  
  
  
Leave your email address when filling out the contact form and we will let you know about the progress of proposed changes.

  


