---
title: Before you start
href: "before_you_start"
created: "2020-10-15"
---

# Before you start

Before you take a deep dive into our documentation, please take a moment to learn some basic information about the technology and tools we are using. It might help you to understand the documents better and faster.

As a cloud forming company, we are trying to leverage every trending tool and technology that would enable continuous delivery at high velocity.

And with that being said, our guiding principles are DevOps and SRE(Site-Reliability-Engineering). DevOps is a cultural philosophy that injects simplicity and open minded approach to company ranks. Together with SRE, which provides guidelines to achieve sustainable high service reliability and availability, they work as a modern strategy for companies to deliver their goals at fastest pace possible.  

<br>
<div class="resp-container">
  <iframe 
    src="https://www.youtube.com/embed/uTEL8Ff1Zvk" 
    frameborder="0" 
    allow="encrypted-media; picture-in-picture" 
    allowfullscreen
    >
  </iframe>
</div>

*This video could provide a quick introduction to both. There are many books, videos and inspirational presentations describing DevOps and SRE from all angles.*  
<br>

There is a popular phrase that DevOps is a set of tools, which is one of the truths about DevOps. So let's introduce some most used technologies and tools we use in Pan-net.  

## List of tools we use

In Pan-net and in DevOps culture generally, we rely heavily on new technologies and tools that improve our efforts in service delivery by leaps and bounds.
Here's a categorized list of most used and needed tools/technologies.  

### Infrastructure

- Openstack - OpenStack is a set of software components that builds a cloud platform. It's a free open standard cloud computing platform developed by community.
**Read more at:** https://docs.openstack.org/ussuri/?_ga=2.58199085.606408823.1602574106-2079154704.1602574106  

- Kubernetes - Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services. It has a large, rapidly growing ecosystem, that we're trying to leverage in Pan-net for every service possible.  
**Read more at:** https://kubernetes.io/docs/home/  

- Docker - Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings.  
**Read more at:** https://docs.docker.com/  

### CICD

- Gitlab - GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration and deployment pipeline features. We rely on Gitlab with all our development and workload management.  
**Read more at:** https://docs.gitlab.com/  

- Artifactory - Artifactory is an universal repository manager supporting all major packaging formats, build tools and CI servers. It extends the functionality of Gitlab as a repository and storage solution.  
**Read more at:** https://www.jfrog.com/confluence/  

- Git - Git is a distributed version-control system for tracking changes in source code during software development. Gitlab relies on git with version-control on it's repositories.  
**Please read more at:** https://git-scm.com/doc  

### Automation

- Ansible - Ansible is an open-source automation tool, or platform, used for IT tasks such as configuration management, application deployment, intraservice orchestration, and provisioning.  
 **Read more at:** https://docs.ansible.com/ansible/latest/index.html  

- Terraform - Terraform is a tool for building, changing, and versioning infrastructure. Terraform can manage existing and popular service providers as well as custom in-house solutions.  
**Read more at:** https://www.terraform.io/docs/index.html  

### Monitoring

- Prometheus - Prometheus is a free software application used for event monitoring and alerting. It records real-time metrics in a time series database built using a HTTP pull model, with flexible queries and real-time alerting.  
**Read more at:** https://prometheus.io/docs/introduction/overview/  

- Grafana - Grafana is open source visualization and analytics software. It allows you to query, visualize, alert on and understand your metrics no matter where they are stored.  
**Read more at:** https://grafana.com/docs/  

### Security

- HashiCorp Vault - Vault is a tool for securely accessing secrets. A secret is anything that you want to tightly control access to, such as API keys, passwords, or certificates.  
**Read more at:** https://www.vaultproject.io/docs/what-is-vault
